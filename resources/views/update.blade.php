@extends('layouts.layout')

@section('content')
  <div class="col-md-8 col-md-push-2">
    <h2>Hi 
      @if ($data['FirstName'])
        {{ $data['FirstName'] }},
      @else
        there,
      @endif
    </h2>
    <div>
      @if (count($errors) > 0)
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
    </div>
    {!! Form::open() !!}
      <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('phone', 'Phone') !!}
          {!! Form::text('phone', $data['Phone'], array('class' => 'form-control')) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('mobile', 'Mobile') !!}
          {!! Form::text('mobile', $data['MobilePhone'], array('class' => 'form-control')) !!}
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          {!! Form::label('email', 'Email Address') !!}
          {!! Form::email('email', $data['Email'], array('class' => 'form-control')) !!}
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          {!! Form::label('mailingstreet', 'Mailing Address') !!}
          {!! Form::text('mailingstreet', $data['MailingStreet'], array('class' => 'form-control')) !!}
          {!! Form::text('mailingcity', $data['MailingCity'], array('class' => 'form-control')) !!}
          {!! Form::select('mailingstate', array(
            'Undefined' => '',
            'Other' => 'Other',
            'Avon' => 'Avon',
            'Bedfordshire' => 'Bedfordshire',
            'Berkshire' => 'Berkshire',
            'Buckinghamshire' => 'Buckinghamshire',
            'Cambridgeshire' => 'Cambridgeshire',
            'Cheshire' => 'Cheshire',
            'Cleveland' => 'Cleveland',
            'Cornwall' => 'Cornwall',
            'Cumberland' => 'Cumberland',
            'Cumbria' => 'Cumbria',
            'Derbyshire' => 'Derbyshire',
            'Devon' => 'Devon',
            'Dorset' => 'Dorset',
            'Durham' => 'Durham',
            'East Suffolk' => 'East Suffolk',
            'East Sussex' => 'East Sussex',
            'Essex' => 'Essex',
            'Gloucestershire' => 'Gloucestershire',
            'Greater London' => 'Greater London',
            'Greater Manchester' => 'Greater Manchester',
            'Hampshire' => 'Hampshire',
            'Hereford and Worcester' => 'Hereford and Worcester',
            'Herefordshire' => 'Herefordshire',
            'Humberside' => 'Humberside',
            'Huntingdon and Peterborough' => 'Huntingdon and Peterborough',
            'Huntingdonshire' => 'Huntingdonshire',
            'Isle of Ely' => 'Isle of Ely',
            'Isle of Wight' => 'Isle of Wight',
            'Kent' => 'Kent',
            'Lancashire' => 'Lancashire',
            'Leicestershire' => 'Leicestershire',
            'Lincolnshire' => 'Lincolnshire',
            'London' => 'London',
            'City of London' => 'City of London',
            'Merseyside' => 'Merseyside',
            'Middlesex' => 'Middlesex',
            'Norfolk' => 'Norfolk',
            'Northamptonshire' => 'Northamptonshire',
            'Northumberland' => 'Northumberland',
            'North Humberside' => 'North Humberside',
            'North Yorkshire' => 'North Yorkshire',
            'Nottinghamshire' => 'Nottinghamshire',
            'Oxfordshire' => 'Oxfordshire',
            'Soke of Peterborough' => 'Soke of Peterborough',
            'Rutland' => 'Rutland',
            'Shropshire' => 'Shropshire',
            'Somerset' => 'Somerset',
            'South Humberside' => 'South Humberside',
            'South Yorkshire' => 'South Yorkshire',
            'Staffordshire' => 'Staffordshire',
            'Suffolk' => 'Suffolk',
            'Surrey' => 'Surrey',
            'Sussex' => 'Sussex',
            'Tyne and Wear' => 'Tyne and Wear',
            'Warwickshire' => 'Warwickshire',
            'West Midlands' => 'West Midlands',
            'Westmorland' => 'Westmorland',
            'West Suffolk' => 'West Suffolk',
            'West Sussex' => 'West Sussex',
            'West Yorkshire' => 'West Yorkshire',
            'Wiltshire' => 'Wiltshire',
            'Worcestershire' => 'Worcestershire',
            'Yorkshire' => 'Yorkshire'
          ), $data['MailingState'], array('class' => 'form-control')) !!}
          {!! Form::text('mailingpostalcode', $data['MailingPostalCode'], array('class' => 'form-control')) !!}
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          {!! Form::label('candidatetype', 'Type') !!}
          {!! Form::select('candidatetype', array(
            'Undefined' => '',
            'Permanent' => 'Permanent',
            'Contract/Interim' => 'Contract/Interim'
          ), $data['type'], array('id' => 'candidatetype', 'class' => 'form-control')) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('function', 'Function') !!}
          {!! Form::select('function', array(
            'Undefined' => '',
            'Contact Centre' => 'Contact Centre',
            'E-commerce' => 'E-commerce',
            'Finance' => 'Finance',
            'Hospitality' => 'Hospitality',
            'HR' => 'HR',
            'IT' => 'IT',
            'Marketing' => 'Marketing',
            'Procurement' => 'Procurement',
            'Retail' => 'Retail',
            'Sales' => 'Sales',
            'Supply Chain' => 'Supply Chain'
          ), null, array('id' => 'function', 'class' => 'form-control')) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          {!! Form::label('specialism', 'Specialism') !!}
          {!! Form::select('specialism', array(), null, array('id' => 'specialism', 'class' => 'form-control')) !!}
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          {!! Form::label('skills', 'Skills') !!}
          {!! Form::textarea('skills', null, array('class' => 'form-control')) !!}
        </div>
      </div>
      <div class="col-md-6">
        <div id="contractcompensation" class="form-group">
          {!! Form::label('dayrate', 'Day Rate') !!}
          <div class="input-group">
            <div class="input-group-addon">£</div>
            {!! Form::number('dayrate', null, array('class' => 'form-control', 'min' => '0', 'max' => '2000', 'step' => '50')) !!}
            <div class="input-group-addon">.00</div>
          </div>
        </div>
        <div id="permcompensation" class="form-group">
          {!! Form::label('salary', 'Salary') !!}
          <div class="input-group">
            <div class="input-group-addon">£</div>
            {!! Form::number('salary', null, array('class' => 'form-control', 'min' => '0', 'max' => '200000', 'step' => '250')) !!}
            <div class="input-group-addon">.00</div>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div id="contractavailability" class="form-group">
          {!! Form::label('contractavailability', 'Availability') !!}
          {!! Form::text('contractavailability', null, array('class' => 'form-control')) !!}
        </div>
        <div id="permavailability" class="form-group">
          {!! Form::label('permavailability', 'Availability') !!}
          {!! Form::select('permavailability', array(
            '' => '',
            'Yes' => 'Yes',
            'No' => 'No'
          ), null, array('class' => 'form-control')) !!}
        </div>
      </div>
      <div class="form-group">
        {!! Form::label('partners', 'Opt in to Partners') !!}
        {!! Form::checkbox('partners', 'partners', true) !!}
      </div>
      {!! Form::submit('Submit', array('class' => 'btn btn-default')) !!}
    {!! Form::close() !!}
  </div>
@endsection
@section('javascript')
  <script type="text/javascript">
    $(document).ready(function() {
      var spec = $("#specialism");

      $("#function").change(function() {
        var funct = $("#function").val(), options;
        spec.find('option').remove();

        switch(funct) {
          case 'Contact Centre':
            options = ['Operations', 'Sales', 'Sales & Service', 'Service']
            break;
          case 'E-commerce':
            options = ['Affiliates', 'Campaign Manager', 'Communications', 'Content', 'CRM', 'Digital Marketing', 'PPC', 'Research', 'SEO', 'Social Media', 'Web Analytics', 'Web Design', 'Web Trading / Merchandise'];
            break;
          case 'Finance':
            options = ['Audit', 'Business Partner', 'Commercial & Analysis', 'Credit Control', 'Due Dilligence', 'Group Accounts', 'Management Accounts', 'Newly Qualified', 'Part Qualified', 'Purchase Ledger', 'Risk', 'Sales Ledger', 'System Accounts', 'Tax', 'Treasury'];
            break;
          case 'Hospitality':
            options = ['Chef', 'Commi Chef'];
            break;
          case 'HR':
            options = ['Comp & Benefits', 'Consultant', 'Learning & Development', 'Organisational Design', 'Recruitment', 'Resourcing', 'Reward', 'Shared Service', 'Talent Management'];
            break;
          case 'IT':
            options = ['Architecture', 'BI / Data Warehousing', 'Business Analyst', 'Cloud', 'Development', 'ERP', 'Infrastructure', 'Networks', 'PMO', 'Programme Management', 'Project Management', 'Security', 'Service Delivery', 'Support Apps', 'Support Infrastructure', 'Tester'];
            break;
          case 'Marketing':
            options = ['Brand', 'Campaign Manager', 'Content', 'Design', 'Events', 'PR'];
            break;
          case 'Procurement':
            options = ['Category Management', 'Direct', 'eProcurement', 'Indirect', 'Merchandising', 'P2P (Procure to Pay)', 'Strategic Sourcing'];
            break;
          case 'Retail':
            options = ['Merchandising', 'Sales', 'Service'];
            break;
          case 'Sales':
            options = ['Account Management', 'Business Development', 'Engineers', 'Internal Sales', 'Technical'];
            break;
          case 'Supply Chain':
            options = ['Customer Service', 'Demand Planning', 'Fulfilment', 'Inventory', 'Logistics', 'Operations', 'Production Planning', 'Reverse Logistics', 'Supply Chain', 'Supply Planning', 'Transport', 'Warehouse'];
            break;
          default:
            options = [''];
        }

        appendOptions(options);
      });

      function appendOptions(options) {
        spec.append('<option value= "Undefined"></option>');

        for (var i = 0; i < options.length; i++) {
          spec.append('<option value= "' + options[i] + '">' + options[i] + '</option>');
        }
      }
    });
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
      var candidatetype = $('#candidatetype'), contractcompensation = $('#contractcompensation'), permcompensation = $('#permcompensation'), contractavailability = $('#contractavailability'), permavailability = $('#permavailability');

      compensation();

      $(candidatetype).change(function() {
        compensation();
      });

      function compensation() {
        var candidatetypevalue = $('#candidatetype').val();

        if (candidatetypevalue == 'Permanent') {
          contractcompensation.hide();
          contractavailability.hide();
          permcompensation.show();
          permavailability.show();
        }
        else if (candidatetypevalue == 'Contract/Interim') {
          contractcompensation.show();
          contractavailability.show();
          permcompensation.hide();
          permavailability.hide();
        }
      }
    });
  </script>
@endsection