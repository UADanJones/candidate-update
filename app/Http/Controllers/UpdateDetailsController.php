<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\UpdateDetailsRequest;

use Input;
use Forrest;
use Carbon\Carbon;

class UpdateDetailsController extends Controller {
  public function index($id) {
    $type = Input::get('type');
    $email = Input::get('email');

    $data = Forrest::query("SELECT FirstName, Phone, MobilePhone, Email, MailingStreet, MailingCity, MailingState, MailingPostalCode, Candidate_Type__c, Function__c, Specialism__c, Hourly_day_rate_to_client__c, ts2__Current_Salary__c, Date_available__c, Not_Looking__c, Partners_Opt_In__c, Partners_Opt_In_Date__c FROM Contact WHERE Id = '".$id."' AND Email = '".$email."' LIMIT 1");

    if ($data['totalSize'] == 0) {
      return view("nocandidate");
    }

    $data['records'][0]['type'] = $type;

    return view("update")->with('data', $data['records'][0]);
  }

  public function update(UpdateDetailsRequest $request, $id) {
    $data = Forrest::query("SELECT Main_Skills__c FROM Contact WHERE Id = '".$id."' LIMIT 1");
    $currentSkills = $data['records'][0]['Main_Skills__c'];

    $phone = str_replace(" ", "", $request->get('phone'));
    $mobile = str_replace(" ", "", $request->get('mobile'));
    $email = str_replace(" ", "", $request->get('email'));
    $mailingstreet = $request->get('mailingstreet');
    $mailingcity = $request->get('mailingcity');
    $mailingstate = $request->get('mailingstate');
    $mailingpostalcode = str_replace(" ", "", $request->get('mailingpostalcode'));
    $candidatetype = $request->get('candidatetype');
    $function = $request->get('function');
    $specialism = $request->get('specialism');
    $skills = $currentSkills.$request->get('skills');
    $dayrate = str_replace(" ", "", $request->get('dayrate'));
    $salary = str_replace(" ", "", $request->get('salary'));
    $permavailable = $this->available($request->get('permavailability'));
    $contractavailable = ($request->get('contractavailability')) ? Carbon::createFromFormat('d/m/Y', $request->get('contractavailability'))->format('Y-m-d') : null;
    $optin = ($request->get('partners') == "partners") ? true : false;
    $optindate = ($request->get('partners')) ? Carbon::now()->format('Y-m-d\TH:i:s\Z') : null;
    
    $body = array();
    if ($phone != null) {
      $body['Phone'] = $phone;
    }
    if ($mobile != null) {
      $body['MobilePhone'] = $mobile;
    }
    if ($email != null) {
      $body['Email'] = $email;
    }
    $body['MailingStreet'] = $mailingstreet;
    $body['MailingCity'] = $mailingcity;
    $body['MailingState'] = $mailingstate;
    if ($mailingpostalcode != null) {
      $body['MailingPostalCode'] = $mailingpostalcode;
    }
    $body['Candidate_Type__c'] = $candidatetype;
    $body['Function__c'] = $function;
    $body['Specialism__c'] = $specialism;
    $body['Main_Skills__c'] = $skills;
    if ($dayrate != null) {
      $body['Hourly_day_rate_to_client__c'] = $dayrate;
    }
    if ($salary != null) {
      $body['ts2__Current_Salary__c'] = $salary;
    }
    $body['Date_available__c'] = $contractavailable;
    $body['Not_Looking__c'] = $permavailable;
    $body['Partners_Opt_In__c'] = $optin;
    $body['Partners_Opt_In_Date__c'] = $optindate;

    Forrest::sobjects('Contact/'.$id,[
      'method' => 'patch',
      'body'   => $body
    ]);

    return view("thanks");
  }

  public function available($available) {
    if ($available == 'Yes') {
      return false;
    }
    else {
      return true;
    }
  }
}
