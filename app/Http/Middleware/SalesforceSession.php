<?php

namespace App\Http\Middleware;

use Closure;
use Forrest;
use Session;

class SalesforceSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Session::has('forrest_token')) {
            Forrest::authenticate();
        }

        return $next($request);
    }
}
