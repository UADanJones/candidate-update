<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateDetailsRequest extends FormRequest {
  public function authorize() {
    return true;
  }

  public function rules() {
    return [
      'phone' => '',
      'mobile' => 'required',
      'email' => 'required',
      'candidatetype' => 'required|in:Undefined,Permanent,Contract/Interim',
      'function' => 'required|in:Undefined,Contact Centre,E-commerce,Finance,Hospitality,HR,IT,Marketing,Procurement,Retail,Sales,Supply Chain',
      'specialism' => 'required|in:Undefined,Operations,Sales,Sales & Service,Service,Affiliates,Campaign Manager,Communications,Content,CRM,Digital Marketing,PPC,Research,SEO,Social Media,Web Analytics,Web Design,Web Trading / Merchandise,Audit,Business Partner,Commercial & Analysis,Credit Control,Due Dilligence,Group Accounts,Management Accounts,Newly Qualified,Part Qualified,Purchase Ledger,Risk,Sales Ledger,System Accounts,Tax,Treasury,Chef,Commi Chef,Comp & Benefits,Consultant,Learning & Development,Organisational Design,Recruitment,Resourcing,Reward,Shared Service,Talent Management,Architecture,BI / Data Warehousing,Business Analyst,Cloud,Development,ERP,Infrastructure,Networks,PMO,Programme Management,Project Management,Security,Service Delivery,Support Apps,Support Infrastructure,Tester,Brand,Design,Events,PR,Category Management,Direct,eProcurement,Indirect,Merchandising,P2P (Procure to Pay),Strategic Sourcing,Account Management,Business Development,Engineers,Internal Sales,Technical,Customer Service,Demand Planning,Fulfilment,Inventory,Logistics,Production Planning,Reverse Logistics,Supply Chain,Supply Planning,Transport,Warehouse',
      'dayrate' => 'numeric',
      'salary' => 'numeric',
      'contractavailability' => 'date_format:d/m/Y',
      'permavailability' => 'in:Undefined,Yes,No'
    ];
  }

  public function messages() {
    return [
      'phone.required' => 'Phone is required!',
      'mobile.required' => 'Mobile is required!',
      'email.required' => 'Email is required!',
      'candidatetype.required' => 'Type is required!',
      'candidatetype.in' => 'Invalid field supplied!',
      'function.required' => 'Function is required!',
      'function.in' => 'Invalid field supplied!',
      'specialism.required' => 'Specialism is required!',
      'specialism.in' => 'Invalid field supplied!',
      'dayrate.numeric' => 'Day Rate must be Numeric!',
      'salary.numeric' => 'Salary must be Numeric!',
      'contractavailability.date_format' => 'Date should be dd/mm/yyyy'
    ];
  }
}